#ifndef _TRFACT_H_
#define _TRFACT_H_
#include <string>
#include <memory>
class Treasure;
typedef Treasure* TreasurePtr;

class TreasureFactory{
public:
	TreasurePtr generateTreasure();
};

#endif