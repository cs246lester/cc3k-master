#ifndef _HUMAN_H_
#define _HUMAN_H_

#include "player.h"
#include <memory>

class Human:public Player {
public:
	Human(int hp=140, int def=20, int attk=20, char symbol='@', int gold=0);
};

#endif