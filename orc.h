#ifndef _ORC_H
#define _ORC_H

#include "player.h"
#include <memory>

class Orc:public Player{
	public:
	Orc(int hp=180, int def=25,int attk=30, char symbol='@',int gold=0);
};

#endif
