#ifndef _WEREWOLF_H_
#define _WEREWOLF_H_
#include "enemy.h"
#include <memory>
class Werewolf : public Enemy {
public:
	Werewolf(int hp=120, int def=5, int attk=30, char symbol='W', int gold=1);
};
#endif