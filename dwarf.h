#ifndef _DWARF_H_
#define _DWARF_H_

#include <memory>
#include "player.h"

class Dwarf:public Player {
public:
	Dwarf(int hp=100, int def=30,  int attk=20, int gold=0);
};

#endif