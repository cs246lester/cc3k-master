#ifndef _PHEALTH_H
#define _PHEALTH_H

#include "potion.h"
#include <memory>
class Player;
class Elf;
typedef Player* PlayerPtr;
typedef Elf* ElfPtr;
class PHealth:public Potion{
public:
	PHealth(char symbol='P');
	void getPickedBy(PlayerPtr player) override;
	void getPickedBy(ElfPtr elf) override;
};

#endif