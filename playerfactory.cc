#include "playerfactory.h"
#include "human.h"
#include "orc.h"
#include "elf.h"
#include "dwarf.h"
#include <memory>
using namespace std;

PlayerPtr PlayerFactory::generatePlayer(char race){
	if(race == 'h'){ // human
		PlayerPtr human = new Human();
		//PlayerPtr human (new Human());
		return human;
	}
	else if(race == 'e'){ // elf
		PlayerPtr elf  = new Elf();
		//PlayerPtr elf (new Elf());
		return elf;
	}
	else if(race == 'd'){ // dwarf
		PlayerPtr dwarf = new Dwarf();
		//PlayerPtr dwarf (new Dwarf());
		return dwarf;
	}
	else { // Orc
		PlayerPtr orc = new Orc();
		//PlayerPtr orc (new Orc());
		return orc;
	}
}
