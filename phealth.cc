#include "phealth.h"
#include "player.h"
#include "elf.h"
#include <iostream>
using namespace std;

const int healthAmt = 10;
const int zeroHealth = 0;

PHealth::PHealth(char symbol): Potion{symbol}{}

void PHealth::getPickedBy(PlayerPtr player){
		cout << "phealth getPickedBy" << endl;
		const int curHealth = player->getHp();
		player->setHp(curHealth - healthAmt); 
		// Note, the condition that a player's health cannot fall below 0 is taken 
		// care of in the setHp method (see setHp in character.cc)

		curCell->setItem(nullptr);
		cout << "cell set to nullptr" << endl;
}

void PHealth::getPickedBy(ElfPtr elf){
	cout << "phealth getPickedBy" << endl;
	// TO DO : need to account for condition "cannot exceed maximum prescribed by race"
	const int curHealth = elf->getHp();
	elf->setHp(curHealth + healthAmt);

	curCell->setItem(nullptr);
	cout << "cell set to nullptr" << endl;
}