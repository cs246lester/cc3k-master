#ifndef _ELF_H_
#define _ELF_H_

#include <memory>
#include "player.h"

class Elf:public Player {
public:
	Elf(int hp=140, int def=10, int attk=30, char symbol= '@', int gold=0);
};

#endif