#ifndef _MERCHANT_H_
#define _MERCHANT_H_

#include <string>
#include <vector>
#include "enemy.h"
#include <memory>

class MerchantHorde;
typedef MerchantHorde* MHPtr;

class Merchant:public Enemy{
	bool hostile;
	MHPtr merchantHorde;
	std::vector<Merchant*> observers;
public:
	Merchant(MHPtr merchantHorde=nullptr, int hp = 30, int def=25, int attk=25, char symbol='M', int gold=0);

	bool isHostile(); // same as getState()
	void attach(Merchant* newOb);
	void notifyObservers();
	void notify(); 
};
typedef Merchant* MerchantPtr;
#endif