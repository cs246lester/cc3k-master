#ifndef _CELL_H_
#define _CELL_H_
#include <string>
#include <map>
#include <memory>
#include <memory>
#include "character.h"

class Item; // forward declaration 
class TextDisplay;
typedef Item* ItemPtr;
typedef TextDisplay* TDPtr;
typedef Cell* CellPtr;
class Cell{
	int row,col;
	char symbol;
	std::map< std::string, CellPtr> neighbours;
	CharacterPtr curCharacter;
	ItemPtr curItem;
	TDPtr td;
public:
	Cell(int r,int c, char symbol); // ctor
	void removeChar();
	int getRow();
	int getCol();
	char getSymbol();
	void setCharacter(CharacterPtr c);
	void setSymbol(char sym);
	CharacterPtr getCharacter();
	void setItem(ItemPtr item);
	ItemPtr getItem();
	void setCoords(int r,int c);
	void notifyDisplay();
	CellPtr getNeighbour(std::string dir);
	void attachDisplay(TDPtr display);
	void attachNeighbour(CellPtr cell, std::string dir);
	~Cell();
};
#endif