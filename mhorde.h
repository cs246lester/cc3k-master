#ifndef _MERCHANTHORDE_H_
#define _MERCHANTHORDE_H_

#include "treasure.h"
#include <string>
#include <memory>

class Cell;
typedef Cell* CellPtr;
class MerchantHorde:public Treasure{
public:
	MerchantHorde(CellPtr cell=nullptr,int gold=4);
};

#endif