#include "dragon.h"
#include "gamestate.h"
#include "dhorde.h"
#include "player.h"
#include <cmath>
#include <iostream>
using namespace std;

Dragon::Dragon(int hp, int def, int attk,char symbol, int gold): Enemy {hp, def, attk, symbol, gold}, hostile{false}, horde {nullptr} {}

bool Dragon::isHostile() {
	return hostile;
}

void Dragon::notify() {
	hostile = not hostile;
}

void Dragon::attachHorde(DHPtr h) {
	horde = h;
}

void Dragon::notifyHorde() {
	horde->notify();
}

void Dragon::getAttackedBy(CharacterPtr attacker) {
	GameState state;
	PlayerPtr castedAttacker = static_cast<PlayerPtr>(attacker);
	int totalattack = castedAttacker->getAttk() + castedAttacker->getTempAttk();
	float damage = (100/(100 + getDef())) * totalattack; 
	setHp(getHp() - ceil(damage));
	state.updateMessage(" Player attacked the dragon and dealt " + to_string(ceil(damage)) + " damage!");
	if (getHp() <= 0){
		state.updateMessage(" Player successfully killed the dragon!");
		state.updateMessage(" Player can now pick up the Dragon Horde!");
		castedAttacker->addGold(gold);
		notifyHorde();
		curCell->setCharacter(nullptr);
		delete this;
	}
}