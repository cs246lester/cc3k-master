CXX = g++
CXXFLAGS = -std=c++14 -g -Werror=vla -MMD
EXEC = cc3k
OBJECTS = main.o chamber.o gamestate.o game.o player.o item.o potion.o dhorde.o battack.o enemyfactory.o mhorde.o shorde.o vampire.o bdefense.o dragon.o floor.o human.o nhorde.o playerfactory.o textdisplay.o wattack.o cell.o dwarf.o orc.o treasure.o wdefense.o cellfactory.o elf.o phealth.o potionfactory.o treasurefactory.o werewolf.o character.o enemy.o goblin.o merchant.o phoenix.o rhealth.o troll.o
DEPENDS = ${OBEJECTS: .o=.d}

${EXEC}: ${OBJECTS}
	${CXX} ${CXXFLAGS} ${OBJECTS} -o ${EXEC}

-include ${DEPENDS}
.PHONY: clean

clean:
	rm ${OBJECTS} ${EXEC} ${DEPENDS}
