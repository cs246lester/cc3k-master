#ifndef _ITEM_H_
#define _ITEM_H_

class Player;

#include <string>
#include "cell.h"
#include <memory>
typedef Cell* CellPtr;
typedef Player* PlayerPtr;
class Item {
protected:
	CellPtr curCell;
	char symbol;
public:
		Item(char symbol,CellPtr cell = nullptr);
		virtual void getPickedBy(PlayerPtr player) = 0;
		char getSymbol();
		void setCurCell(CellPtr cell);
		virtual ~Item();
};
#endif