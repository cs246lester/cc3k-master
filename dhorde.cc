#include "dhorde.h"
#include "gamestate.h"
#include "dragon.h"
#include "cell.h"
#include <iostream>
#include "orc.h"
#include "dwarf.h"
#include <string>
using namespace std;

DragonHorde::DragonHorde(CellPtr cell, DragonPtr dragon): Treasure {6,cell}, dragon{dragon} {}

void DragonHorde::attach(DragonPtr dragon) {
	this->dragon = dragon;
}

void DragonHorde::notifyDragon() {
	dragon->notify();
}

void DragonHorde::notify() {
	canBePicked = true;
}

void DragonHorde::detectIntrusion() {
	// We check all the neighbouring cells to see if the Player is on any one of them
	string directions[] = {"no", "sw", "so", "ea", "we", "nw", "ne", "se"};
	for (int i = 0; i < 8; ++i) {
		if (curCell->getNeighbour(directions[i]) and curCell->getNeighbour(directions[i])->getSymbol() == '@') {
			if (not dragon->isHostile()) notifyDragon();
			return;
		}
	}
	// If we didnt find the Player on any of the neighbouring cells,
	// We check if the dragon isHostile, if it is, we notify it again.
	if (dragon->isHostile()) notifyDragon();
}

void DragonHorde::getPickedBy(PlayerPtr player) {
	GameState state;
	if (not canBePicked) {
		state.updateMessage(" Player needs to slay the dragon to pick up the Dragon Horde!");
	} else {
		player->addGold(hordeVal);
		setCurCell(nullptr);
		delete this;
	}
}
void DragonHorde::getPickedBy(OrcPtr player) {
	GameState state;
	if (not canBePicked) {
		state.updateMessage(" Player needs to slay the dragon to pick up the Dragon Horde!");
	} else {
		player->addGold(0.5 * hordeVal);
		setCurCell(nullptr);
		delete this;
	}
}
void DragonHorde::getPickedBy(DwarfPtr player) {
	GameState state;
	if (not canBePicked) {
		state.updateMessage(" Player needs to slay the dragon to pick up the Dragon Horde!");
	} else {
		player->addGold(2 * hordeVal);
		setCurCell(nullptr);
		delete this;
	}
}