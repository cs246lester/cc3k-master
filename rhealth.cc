#include "rhealth.h"
#include "player.h"
#include "elf.h"
#include <iostream>
using namespace std;

const int healthAmt = 10;

RHealth::RHealth(char symbol): Potion{symbol}{}

void RHealth::getPickedBy(PlayerPtr player){
	const int curHealth = player->getHp();
	player->setHp(curHealth + healthAmt); 
	// TO DO : need to account for condition "cannot exceed maximum prescribed by race"

	curCell->setItem(nullptr);
}

void RHealth::getPickedBy(ElfPtr elf){
	// TO DO : need to account for condition "cannot exceed maximum prescribed by race"
	const int curHealth = elf->getHp();
	elf->setHp(curHealth + healthAmt);

	curCell->setItem(nullptr);
}