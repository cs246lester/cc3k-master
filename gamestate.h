#ifndef _GAMESTATE_H_
#define _GAMESTATE_H_
#include <string>
#include <memory>
// This is a singleton class to keep track of the game's state.

class GameState {
	static std::string message;
	static bool alive;
	static bool levelEnded;
	static bool gameEnded;
public:
	void updateMessage(std::string newMessage);
	void clearMessage();
	std::string getMessage();
	bool isAlive();
	void setAlive(bool life);
	bool isLevelEnded();
	void setLevelEnded(bool ended);
	bool isGameEnded();
	void setGameEnded(bool endgame);
	GameState();
};
#endif