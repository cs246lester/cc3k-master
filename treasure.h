#ifndef _TREASURE_H_
#define _TREASURE_H_
#include <string>
#include <memory>
class Character;
class Dwarf;
class Orc;
class Player;
class Cell;
typedef Player* PlayerPtr;
typedef Dwarf* DwarfPtr;
typedef Orc* OrcPtr;
typedef Cell* CellPtr;
#include "item.h"

class Treasure : public Item {
protected:
	int hordeVal;
public:
	int getHordeVal();
	Treasure(int gold=1, CellPtr cell = nullptr, char symbol = 'G');
	virtual void getPickedBy(PlayerPtr player) override;
	virtual void getPickedBy(DwarfPtr dwarf);
	virtual void getPickedBy(OrcPtr orc);
	virtual ~Treasure();
};

#endif