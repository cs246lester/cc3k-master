#ifndef _CHAMBER_H_
#define _CHAMBER_H_
#include <memory>
#include <vector>
class Cell;
typedef Cell* CellPtr;
class Chamber {
	std::vector<CellPtr> chamberCells;
public:
	void attach(CellPtr cell);
	CellPtr getCell(int index);
	int getSize();
	bool getHasPlayer();
};
#endif