#include "chamber.h"
#include "cell.h"
#include <iostream>
using namespace std;

void Chamber::attach(CellPtr cell) {
	chamberCells.emplace_back(cell);
}

CellPtr Chamber::getCell(int index) {
	return chamberCells[index];
}

int Chamber::getSize() {
	return chamberCells.size();
}

bool Chamber::getHasPlayer() {
	for (int i = 0; i < chamberCells.size(); ++i) {
		if (chamberCells[i]->getSymbol() == '@') {
			return true;
		}
	}
	return false;
}
