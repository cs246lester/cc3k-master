#include "floor.h"
#include "dragon.h"
#include "dhorde.h"
#include "chamber.h"
#include "enemyfactory.h"
#include "potionfactory.h"
#include "treasurefactory.h"
#include "potion.h"
#include "player.h"
#include <cstdlib>
#include <ctime>
#include <iostream>
#include <random>
using namespace std;
typedef Character* CharacterPtr;
typedef Potion* PotionPtr;
typedef Item* ItemPtr;
typedef Enemy* EnemyPtr;
typedef Dragon* DragonPtr;
typedef DragonHorde* DHPtr;

Floor::Floor(PlayerPtr pc, vector < vector < CellPtr > > theGrid): player {pc}, theGrid {theGrid} {
	// First we create the five chambers
	for (int i = 0; i < rows; ++i) {
		std::vector<bool> vec;
		tr.emplace_back(vec);
		for (int j = 0; j < cols; ++j) {
			tr[i].emplace_back(false);
		}
	}
	for (int i = 0; i < 5; ++i) {
		ChamberPtr newChamber = new Chamber();
		chambers.emplace_back(newChamber);
		populate(newChamber);
	}

	playerSpawn();
	stairwaySpawn();
	potionSpawn();
	treasureSpawn();
	enemySpawn();
}

Floor::~Floor() {
	for (int i = 0; i < chambers.size(); ++i) {
		delete chambers[i];
	}
}

void Floor::populate(ChamberPtr chamber) {
	for (int i = 0; i < rows; ++i) {
		for (int j = 0; j < cols; ++j) {
			CellPtr curCell = theGrid[i][j];
			if (curCell->getSymbol() == '.' and tr[i][j] == false) {
				// As soon as we encounter an empty new tile, we expand it and then return
				// so as to populate the next chamber.
				expand(curCell, chamber); return;
			}
		}
	}
}

void Floor::expand(CellPtr curCell, ChamberPtr chamber) {
	int row = curCell->getRow();
	int col = curCell->getCol();
	chamber->attach(curCell);
	tr[row][col] = true;
	string directions[8] = {"no", "se", "so", "sw", "ea", "we", "ne", "nw"};
	for (int i = 0; i < 8; ++i) {
		CellPtr nextCell = curCell->getNeighbour(directions[i]);
		// if the nextCell is a tile and its not in any other chamber
		if (nextCell and nextCell->getSymbol() == '.' and tr[nextCell->getRow()][nextCell->getCol()] == false) {
			expand(nextCell, chamber);
		}
	}
}

void Floor::playerSpawn() {
	// We generate a random chamber number

	int chamberNumber = rand() % 5;
	ChamberPtr theChamber = chambers[chamberNumber];
	int chamberSize = theChamber->getSize();
	CellPtr playerCell = theChamber->getCell(rand() % chamberSize);
	CharacterPtr castedPlayer = static_cast<CharacterPtr> (player);
	playerCell->setCharacter(castedPlayer);
	castedPlayer->setCurCell(playerCell);
}

void Floor::stairwaySpawn() {
	// srand(time(nullptr));
	// First we find a chamber that does not have the Player
	int chamberNumber = rand() % 5;
	while (chambers[chamberNumber]->getHasPlayer()) {
		chamberNumber = rand() % 5;
	}
	// So at this stage, we have found a chamber that does not have the Player
	ChamberPtr stairChamber = chambers[chamberNumber];
	CellPtr sCell = stairChamber->getCell(rand() % stairChamber->getSize());
	sCell->setSymbol('\\');
}

void Floor::potionSpawn() {
	PotionFactory pfactory;
	const int totalPotions = 10;
	for (int i = 0; i < totalPotions; ++i) {
		int chamberNumber = rand() % 5;
		ChamberPtr theChamber = chambers[chamberNumber];
		int chamberSize = theChamber->getSize();
		CellPtr chamberCell = theChamber->getCell(rand() % chamberSize);
		while (chamberCell->getSymbol() != '.') {
			int randnum = rand()%chamberSize;
			chamberCell = theChamber->getCell(randnum);
		}
		// At this stage, we have a random empty Cell in a random Chamber
		PotionPtr potion = pfactory.generatePotion();
		ItemPtr castedPotion = static_cast<ItemPtr>(potion);
		chamberCell->setItem(castedPotion); castedPotion->setCurCell(chamberCell);
	}
}

void Floor::treasureSpawn() {
	TreasureFactory tfactory;
	const int totalGold = 9;
	for (int i = 0; i < totalGold; ++i) {
		int chamberNumber = rand() % 5;
		ChamberPtr theChamber = chambers[chamberNumber];
		int chamberSize = theChamber->getSize();
		CellPtr chamberCell = theChamber->getCell(rand() % chamberSize);
		while (chamberCell->getSymbol() != '.') {
			chamberCell = theChamber->getCell(rand() % chamberSize);
		}
		// At this stage we have a random empty Cell in a random Chamber

		int random = rand() % 8;
		if (random == 7) {
			dragonSpawn(chamberCell);
		} else {
			ItemPtr horde = tfactory.generateTreasure();
			chamberCell->setItem(horde); horde->setCurCell(chamberCell);
		}
	}
}

void Floor::enemySpawn() {
	EnemyFactory enemyFactory;
	const int totalEnemies = 20;
	for (int i = 0; i < totalEnemies; ++i) {
		int chamberNumber = rand() % 5;
		
		ChamberPtr theChamber = chambers[chamberNumber];
		// At this stage, we've selected a chamber
		int chamberSize = theChamber->getSize();
		CellPtr chamberCell = theChamber->getCell(rand() % chamberSize);
		while (chamberCell->getSymbol() != '.') {
			chamberCell = theChamber->getCell(rand() % chamberSize);
		}
		// Now we have a random empty Cell
		CharacterPtr enemy = enemyFactory.generateEnemy();
		chamberCell->setCharacter(enemy);
		enemy->setCurCell(chamberCell);
		EnemyPtr castedEnemy = static_cast<EnemyPtr>(enemy);
		enemies.emplace_back(castedEnemy);
	}
}

void Floor::dragonSpawn(CellPtr chamberCell) {
	// First we create a dragonHorde and a dragon on the heap
	DragonPtr dragon = new Dragon ();
	DHPtr dragonHorde = new DragonHorde (chamberCell, dragon);
	dragonHorde->attach(dragon);
	chamberCell->setItem(dragonHorde); 
	dragon->attachHorde(dragonHorde);
	string directions[8] = {"no", "so", "se", "ea", "we", "nw", "ne", "sw"};
	// Now we find a neighbouring cell that is empty, and we instantiate a dragon on it
	for (int i = 0; i < 8; ++i) {
		CellPtr dragonCell = chamberCell->getNeighbour(directions[i]);
		if (dragonCell->getSymbol() == '.') {
			dragonCell->setCharacter(dragon); dragon->setCurCell(dragonCell); break;
		}
	}
}

vector<EnemyPtr> Floor::getEnemies() {
	return enemies;
}