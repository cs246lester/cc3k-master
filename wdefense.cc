#include "wdefense.h"
#include "player.h"
#include "elf.h"
#include <iostream>
using namespace std;

const int defenseAmt = 5;

WDefense::WDefense(char symbol): Potion{symbol}{}

void WDefense::getPickedBy(PlayerPtr player){
	cout << "wdefense getPickedBy" << endl;
	const int totalDef = player->getDef() + player->getTempDef();
	if(totalDef - defenseAmt > 0)
		player->updateTempDef(-1*defenseAmt);

	else
		player->setTempDef(-1 * player->getDef()); // to nullify the defense of the character (zero defense)
			
		curCell->setItem(nullptr);	
		cout << "cell set to nullptr" << endl;
		delete this;
}

void WDefense::getPickedBy(ElfPtr elf){
	cout << "wdefense getPickedBy" << endl;
	elf->updateTempDef(defenseAmt);

	curCell->setItem(nullptr);
	cout << "cell set to nullptr" << endl;
	delete this;
}