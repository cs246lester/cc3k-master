#include <cmath>
#include <iostream>
#include <cstdlib>
#include "merchant.h"
#include "enemy.h"
#include "cell.h"
#include "gamestate.h"
#include "player.h"
#include <ctime>
#include <iostream>
#include <string>
#include <memory>
using namespace std;
typedef Player* PlayerPtr;
typedef Merchant* MerchantPtr;
typedef Cell* CellPtr;

Enemy::Enemy(int hp, int def, int attk, char symbol, int gold): Character(hp,def,attk,symbol,gold){

}

Enemy::~Enemy() {}

void Enemy::getAttackedBy(CharacterPtr attacker) {
	GameState state;
	PlayerPtr castedAttacker = static_cast<PlayerPtr>(attacker);
	if(curCell->getSymbol() == 'M'){ // if the enemy that is being attacked is a merchant
		 MerchantPtr merchant = static_cast<MerchantPtr>(curCell->getCharacter());
		if (not merchant->isHostile()) {
			state.updateMessage(" Player attacked a merchant. All the merchants are now hostile!");
		}
		merchant->notifyObservers(); // makes all merchants hostile
	}

	int totalattack = castedAttacker->getAttk() + castedAttacker->getTempAttk();
	float damage = (100/(100 + getDef())) * totalattack; 
	setHp(getHp() - ceil(damage));
	state.updateMessage(" Player dealt " + to_string(ceil(damage)) + " damage!");
	if (getHp() <= 0){
		state.updateMessage(" Player successfully killed the enemy!");
		castedAttacker->addGold(gold);
		curCell->setCharacter(nullptr);
		delete this;
	}
}

void Enemy::navigate(){
	// Enemy will first look for the Player character. If it finds it, it'll attack the player and 
	// wont move 
	if (lookForPlayer()) return;

	// If the Enemy doesnt find the Player character in the neighbouring cells, it randomly move onto 
	// one of those cells.
	string indexToDirection[8] = {"no", "so", "sw", "ea", "nw", "ne", "se", "we"};
	bool cornered = true;
	for (int i = 0; i < 8; ++i) {
		CellPtr nextCell = curCell->getNeighbour(indexToDirection[i]);
		if (nextCell != nullptr and nextCell->getSymbol() == '.')
			cornered = false;
	}
	if (cornered)
		return;
	int index = rand() % 8;
	CellPtr nextCell = curCell->getNeighbour(indexToDirection[index]);
	while (nextCell == nullptr or nextCell->getSymbol() != '.') {
		index = rand() % 8;
		nextCell = curCell->getNeighbour(indexToDirection[index]);
	}
	move(indexToDirection[index]);
}

bool Enemy::isHostile() {
	return true;
}

bool Enemy::lookForPlayer() {
	// Enemy scans the neighbouring cells for the Player Character and attacks it 
	// if it finds the Player on one of these cells.
	string directions[8] = {"no", "so", "sw", "ea", "nw", "ne", "se", "we"};
	for (int i = 0; i < 8; ++i) {
		string dir = directions[i];
		CellPtr nextCell = curCell->getNeighbour(dir);
		if (nextCell and nextCell->getSymbol() == '@') {
			attack(dir); return true;
		}
	}
	return false;
}

void Enemy::attack(string dir){
	GameState state;
	CellPtr nextCell = curCell->getNeighbour(dir);
	if(nextCell == nullptr){
		state.updateMessage(" The cell is a nullptr!");
		return;
	}

	if(nextCell->getCharacter() == nullptr){
		state.updateMessage(" There is no one to attack!");
		return;
	}

	if(nextCell->getSymbol() != '@'){
		state.updateMessage(" The enemy attacked the Player!");
	}

	// if we get here, that means nextCell contains a character and the character is a player

	
	CharacterPtr player= nextCell->getCharacter(); // character on the nextCell
	player->getAttackedBy(this);
}