#ifndef _CELLFACTORY_H_
#define _CELLFACTORY_H_
#include "cell.h"
#include <memory>
class CellFactory {
public:
	CellPtr generateCell(int row, int col, char symbol);
};
#endif