#ifndef _CHARACTER_H_
#define _CHARACTER_H_
#include <string>
#include <memory>
class Cell;
class Enemy;
typedef Cell* CellPtr;
typedef Enemy* EnemyPtr;


class Character {
	private:
		int hp;
		int def;
		int attk;
		bool alive;
		char symbol;
	protected:
		int gold;
		CellPtr curCell;
	public:
		Character(int hp, int def, int attk, char symbol, int gold);
		int getHp() const;
		int getAttk() const;
		int getDef() const;
		bool isAlive() const;
		void setCurCell(CellPtr c);
		void addGold(int plusgold);
		// TODO: Write these functions in character.cc
		void setHp(int hp);
		char getSymbol();
		void setAttk(int attk);
		void setDef(int def);
		void setAlive(bool alive);
		virtual void move(std::string dir);
		virtual void attack(std::string dir) = 0;
		virtual ~Character();
		virtual void getAttackedBy(Character* attacker) = 0;
};
typedef Character* CharacterPtr;
#endif
