#ifndef _WDEF_H
#define _WDEF_H

#include "potion.h"
#include <memory>
class Player;
class Elf;
typedef Elf* ElfPtr;
typedef Player* PlayerPtr;

class WDefense:public Potion{
public:
	WDefense(char symbol = 'P');
	void getPickedBy(PlayerPtr player) override;
	void getPickedBy(ElfPtr elf) override;
};

#endif