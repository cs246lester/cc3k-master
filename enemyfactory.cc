#include "enemyfactory.h"
#include "vampire.h"
#include "werewolf.h"
#include "goblin.h"
#include "troll.h"
#include "phoenix.h"
#include "merchant.h"
#include "enemy.h"
#include "mhorde.h"
#include <iostream>
#include <ctime>
#include <cstdlib>
#include <utility>
using namespace std;
	

EnemyPtr EnemyFactory::generateEnemy(){
	int enemyNumber = rand() % 18;
	if(enemyNumber == 0 || enemyNumber == 1 || enemyNumber == 2 || enemyNumber == 3){
		EnemyPtr werewolf = new Werewolf();
		return werewolf; 
	}

	else if(enemyNumber == 4 || enemyNumber == 5 || enemyNumber == 6){
		EnemyPtr vampire = new Vampire();
		return vampire;
	}
	else if(enemyNumber == 7 || enemyNumber == 8 || enemyNumber == 9 || 
		    enemyNumber == 10 || enemyNumber == 11){
		EnemyPtr goblin = new Goblin();
		return goblin;
	}

	else if(enemyNumber == 12 || enemyNumber == 13){
		EnemyPtr troll = new Troll();
		return troll;
	}

	else if(enemyNumber == 14 || enemyNumber == 15){
		EnemyPtr phoenix = new Phoenix();
		return phoenix;
	}

	else {
		shared_ptr<MerchantHorde> merchantHorde = make_shared<MerchantHorde>();
		EnemyPtr merchant = new Merchant();
		return merchant;
	}
}