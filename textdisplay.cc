#include "textdisplay.h"
#include "cell.h"
#include <iostream>
#include <vector>

using namespace std;

// Constructing the TextDisplay initially to hold just '!'s
TextDisplay::TextDisplay() {
	for (int i = 0; i < rows; ++i) {
		vector<char> newVec;
		theDisplay.emplace_back(newVec);
		for (int j = 0; j < cols; ++j) {
			theDisplay[i].emplace_back('!');
		}
	}
}

void TextDisplay::notify(Cell& c) {
	int row = c.getRow();
	int column = c.getCol();
	char symbol = c.getSymbol();
	theDisplay[row][column] = symbol;
}

ostream& operator<<(ostream& out, const TextDisplay& td) {
	for (int i = 0; i < td.rows; ++i) {
		for (int j = 0; j < td.cols; ++j) {
			out << td.theDisplay[i][j];
		}
		out << endl;
	}
	return out;
}