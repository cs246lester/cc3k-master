#include "merchant.h"
#include <iostream>
using namespace std;

Merchant::Merchant(MHPtr merchantHorde,
	int hp, int def, int attk, char symbol, int gold) : merchantHorde{merchantHorde},
	 Enemy{hp,def,attk,symbol,gold}, hostile {false} {

}

bool Merchant::isHostile(){
	return hostile;
}

void Merchant::attach(MerchantPtr newOb){
	observers.emplace_back(newOb);
}

void Merchant::notifyObservers(){
	for(auto& ob : observers){
		ob->notify();
	} 
}

void Merchant::notify(){
	hostile = true;
}

// Line of thought: 
// when getAttackedBy is called on a Merchant, we need to invoke notifyObservers