#ifndef _ENEMY_H_
#define _ENEMY_H_
#include "character.h"
#include <memory>
#include <string>
class Treasure;
class Player;

typedef Player* PlayerPtr;
typedef Treasure* TreasurePtr;
typedef Character* CharacterPtr;
class Enemy : public Character {
	bool hostile;
	public:
		Enemy(int hp, int def, int attk, char symbol, int gold);
		// TODO: Need to write an empty dtor in enemy.cc
		virtual void getAttackedBy(CharacterPtr attacker) override;
		// void setTreasure(Treasure& horde);
		TreasurePtr getTreasure();
		void navigate();
		bool lookForPlayer();
		void attack(std::string dir) override;
		virtual bool isHostile();
		virtual ~Enemy();
};
#endif