#ifndef _TROLL_H_
#define _TROLL_H_

#include <memory>
#include "enemy.h"

class Troll:public Enemy{
public:
	Troll(int hp=120, int def=15, int attk=25, char symbol='T', int gold=1);
};

#endif