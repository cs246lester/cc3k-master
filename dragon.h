#ifndef _DRAGON_H_
#define _DRAGON_H_

#include <memory>
#include "enemy.h"
class DragonHorde;
class Character;
typedef DragonHorde* DHPtr;
typedef Character* CharacterPtr;

class Dragon:public Enemy{
	bool hostile;
	DHPtr horde;
public:
	Dragon(int hp=150, int def=20, int attk=20,char symbol='D', int gold=0);
	bool isHostile() override;
	void notify();
	void attachHorde(DHPtr h);
	void getAttackedBy(CharacterPtr attacker) override;
	void notifyHorde();
};
	
#endif