#ifndef _WATTACK_H
#define _WATTACK_H

class Elf;
#include "potion.h"
#include <memory>
class Player;
typedef Player* PlayerPtr;
typedef Elf* ElfPtr;
class WAttack:public Potion{
public:
	WAttack(char symbol='P');
	void getPickedBy(PlayerPtr player) override;
	void getPickedBy(ElfPtr elf) override;
};

#endif