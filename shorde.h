#ifndef _SMALLHORDE_H_
#define _SMALLHORDE_H_
#include <string>
#include "treasure.h"
#include <memory>

class SmallHorde: public Treasure{
public:
	SmallHorde(int gold = 2);
};

#endif