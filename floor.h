#ifndef _FLOOR_H_
#define _FLOOR_H_
#include <vector>
#include <string>
#include <memory>
class Chamber;
class Cell;
class Player;
class Enemy;
typedef Enemy* EnemyPtr;
typedef Cell* CellPtr;
typedef Chamber* ChamberPtr;
typedef Player* PlayerPtr;

class Floor {
	const int rows = 25;
	const int cols = 79;
	std::vector < ChamberPtr > chambers;
	std::vector < EnemyPtr > enemies;
	PlayerPtr player;
	std::vector < std::vector < CellPtr > > theGrid;
	// vector to keep track of Cells that have already been added to a Chamber
	std::vector< std::vector<bool> > tr;
	static int floorNum;
	void playerSpawn(); // 1st spawn
	void stairwaySpawn(); // 2nd
	void potionSpawn(); // 3rd
	void treasureSpawn(); // 4th
	void enemySpawn(); // 5th
	int getFloorNum();
	void populate(ChamberPtr chamber);
	void dragonSpawn(CellPtr chamberCell);
	void expand(CellPtr, ChamberPtr);
public:
	std::vector< EnemyPtr > getEnemies();
	Floor(PlayerPtr pc, std::vector < std::vector < CellPtr > > theGrid);
	~Floor();
};
#endif