#ifndef _BATTACK_H
#define _BATTACK_H

#include <memory>

class Player;
class Elf;
typedef Player* PlayerPtr;
typedef Elf* ElfPtr;
#include "potion.h"

class BAttack:public Potion{
public:
	BAttack(char symbol='P');
	void getPickedBy(PlayerPtr player) override;
	void getPickedBy(ElfPtr elf) override;
};

#endif