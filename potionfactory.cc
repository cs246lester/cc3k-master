#include "potionfactory.h"
#include "potion.h"
#include "phealth.h"
#include "rhealth.h"
#include "wattack.h"
#include "wdefense.h"
#include "battack.h"
#include <ctime>
#include <cstdlib>
#include "bdefense.h"
using namespace std;

PotionPtr PotionFactory::generatePotion(){
	int potionNum = rand() % 6;
	if(potionNum == 0){ // RH
		PotionPtr p = new RHealth ();
		return p;
	}
	else if(potionNum == 1){ // BA
		PotionPtr p = new BAttack ();
		return p;
	}
	else if(potionNum == 2){ // BD
		PotionPtr p = new BDefense ();
		return p;
	}
	else if(potionNum == 3){ // PH
		PotionPtr p = new PHealth ();
		return p;
	}
	else if(potionNum == 4){ // WA
		PotionPtr p = new WAttack ();
		return p;
	}
	else { // WD
		PotionPtr p = new WDefense ();
		return p;
	}
}