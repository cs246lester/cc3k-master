#ifndef _VAMPIRE_H_
#define _VAMPIRE_H_
#include "enemy.h"
#include <memory>
class Vampire : public Enemy {
public:
	Vampire(int hp=50, int def=25, int attk=25, char symbol='V', int gold=1);
};
#endif