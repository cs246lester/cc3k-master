// Every player has a cell pointer
// elf.move(); should update the current cell of elf to the correct one
// check for neighbouring cells

#include "player.h"
#include "enemy.h"
#include "gamestate.h"
#include <map>
#include <string>
#include <iostream>
#include <cmath>
#include "item.h"
using namespace std;

typedef Item* ItemPtr;

Player::Player(int hp, int def, int attk, char symbol, int gold): Character{hp,def,attk,symbol, gold}, tempAttk{0}, tempDef{0}{
	cout << "player ctor runs" << endl;
}

int Player::getTempAttk(){
	return tempAttk;
}

void Player::updateTempAttk(int attack){
	tempAttk+=attack;
}

int Player::getTempDef(){
	return tempDef;
}

void Player::updateTempDef(int defense){
	tempDef+=defense;
}

void Player::setTempDef(int defense){
	tempDef = defense;
}

void Player::move(string dir) {
	CharacterPtr curChar = this;
	GameState state;
	CellPtr nextCell = curCell->getNeighbour(dir);
	if (nextCell == nullptr or (nextCell->getSymbol() != '.' and nextCell->getSymbol() != '\\' and nextCell->getSymbol() != '#' and nextCell->getSymbol() != '+')) {
		char cellSym = nextCell->getSymbol();
		if (cellSym == 'M' || cellSym == 'D' || cellSym == 'N' || cellSym == 'X' || cellSym == 'T' || cellSym == 'W') {
			state.updateMessage(" Player collided into an enemy!");
		}
		return;
	} else if (nextCell->getSymbol() == '\\') {
		state.updateMessage(" Player proceeded to the next level!");
		state.setLevelEnded(true);
	} else {
		// nextCell->setCharacter(curChar);
		// curChar->setCurCell(nextCell);
		curCell->removeChar();
		curCell = nextCell;
		nextCell->setCharacter(curChar);
		curChar->setCurCell(nextCell);
	}
}

void Player::setTempAttk(int attack){
	tempAttk = attack;
}

void Player::pick(string dir){
	GameState state;
	cout << "pick entered" << endl;
	CellPtr itemCell = curCell->getNeighbour(dir);
	cout << "itemcell acquired" << endl;
	if(itemCell == nullptr){
		state.updateMessage(" There is no tile in that direction!");
		return;
	}

	ItemPtr item = itemCell->getItem();
	cout << "item acquired from itemcell" << endl;
	if(item == nullptr){
		state.updateMessage(" There is no item to pick!");
		return;
	}
	item->getPickedBy(this); // calls the right method depending on type
}

void Player::addGold(int coins) {
	this->gold += coins;
}

void Player::getAttackedBy(CharacterPtr attacker) {
	GameState state;
	float dmg = (100/(100 + getDef()) * attacker->getAttk());
	setHp(getHp() - ceil(dmg));
	string damageTaken = " Player took " + to_string(ceil(dmg)) + " damage";
	state.updateMessage(damageTaken);
	if (getHp() <= 0) {
		state.setAlive(false);
	}
}

void Player::attack (string dir) {
	GameState state;
	CellPtr nextCell = curCell->getNeighbour(dir);
	if (nextCell == nullptr){
	 state.updateMessage(" ERROR! This tile doesnt exist!"); 
	 return;
	}

	CharacterPtr defender = nextCell->getCharacter();
	if (defender == nullptr) {
		state.updateMessage(" There is no enemy in that direction!");
		return;
	}

	defender->getAttackedBy(this);
}