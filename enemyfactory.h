#ifndef _EFACT_H_
#define _EFACT_H_
#include <string>

#include <memory>
class Enemy; 
typedef Enemy* EnemyPtr;
class EnemyFactory{
public:
	EnemyPtr generateEnemy(); 
};

#endif