#include "game.h"
#include "textdisplay.h"
#include "floor.h"
#include "player.h"
#include "gamestate.h"
#include "cell.h"
#include "cellfactory.h"
#include "enemy.h"
#include <fstream>
#include <memory>
#include <iostream>

using namespace std;

Game::~Game() {
	delete td;
	for (int i = 0; i < theGrid.size(); ++i) {
		for (int j = 0; j < theGrid[i].size(); ++j) {
			delete theGrid[i][j];
		}
	}
}

Game::Game(PlayerPtr pc, string filename): filename{filename}, pc {pc} {
	ifstream in {filename};
	td = new TextDisplay ();
	// Reading the file and instantiating the Cell grid
	for (int i = 0; i < rows; ++i) {
		string curline;
		getline(in, curline);
		CellFactory cellFactory;
		std::vector<CellPtr> cellvec;
		theGrid.emplace_back(cellvec);
		for (int j = 0; j < curline.length(); ++j) {
			char cellType = curline[j];
			CellPtr curCell = cellFactory.generateCell(i, j, cellType);
			curCell->attachDisplay(td);
			theGrid[i].emplace_back(curCell);
		}
	}

	// Now we attach the neighbours to each of the Cells
	for (int i = 0; i < rows; ++i) {
		for (int j = 0; j < cols; ++j) {
			if (i != rows-1) {
				theGrid[i][j]->attachNeighbour(theGrid[i+1][j], "so");
			} 
			if (i != rows-1 and j != 0) {
				theGrid[i][j]->attachNeighbour(theGrid[i+1][j-1], "sw");
			} 
			if (j != 0) {
				theGrid[i][j]->attachNeighbour(theGrid[i][j-1], "we");
			}
			if (i != 0 and j != 0) {
				theGrid[i][j]->attachNeighbour(theGrid[i-1][j-1], "nw");
			}
			if (i != 0) {
				theGrid[i][j]->attachNeighbour(theGrid[i-1][j], "no");
			} 
			if (i != 0 and j != cols-1) {
				theGrid[i][j]->attachNeighbour(theGrid[i-1][j+1], "ne");
			}
			if (j != cols-1) {
				theGrid[i][j]->attachNeighbour(theGrid[i][j+1], "ea");
			}
			if (i != rows-1 and j != cols-1) {
				theGrid[i][j]->attachNeighbour(theGrid[i+1][j+1], "se");
			}
		}
	}
	floor = new Floor (pc, theGrid);
}

void Game::respawn() {
	// Have to check if the game has ended, if it has, we need to destroy the cells.
	pc->setTempDef(0); pc->setTempAttk(0); 
	delete floor;
	floor = new Floor (pc, theGrid);
}

// Refreshes the TextDisplay
void Game::notifyDisplay() {
	for (int i = 0; i< rows; ++i) {
		for (int j = 0; j < cols; ++j) {
			theGrid[i][j]->notifyDisplay();
		}
	}
}

void Game::updateState() {
	vector<EnemyPtr> enemies = floor->getEnemies();
	int len = enemies.size();
	for(int i=0;i<len;i++){
		enemies[i]->navigate();
	}
}

ostream& operator<< (ostream& out, Game& game) {
	out << *(game.td);
	return out;
}