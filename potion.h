#ifndef _POTION_H
#define _POTION_H

#include <string>
#include "item.h"
#include <memory>

class Player;
class Elf;
typedef Player* PlayerPtr;
typedef Elf* ElfPtr;
class Potion:public Item{
public:
	Potion(char symbol='P');
	// MIGHT NOT NEED THIS METHOD FOR POSITIVE POTIONS
	virtual void getPickedBy(PlayerPtr player)=0;
	virtual void getPickedBy(ElfPtr elf)=0;
	virtual ~Potion();
};

#endif
