#ifndef _PLAYER_H_
#define _PLAYER_H_
#include <string>
#include "character.h"
#include <memory>
class Enemy;
typedef Character* CharacterPtr;
typedef Enemy* EnemyPtr;

class Player : public Character {
	int tempAttk;
	int tempDef;
	public:
		Player(int hp, int def, int attk, char symbol='@', int gold=0);
		int getTempAttk();
		void updateTempAttk(int attack);
		int getTempDef();
		void addGold(int gold);
		void updateTempDef(int defense);
		void setTempDef(int defense);
		void setTempAttk(int attack);
		void pick(std::string dir); // for both gold and potion
		void getAttackedBy(CharacterPtr attacker) override;
		void move(std::string dir) override;
		void attack(std::string dir) override;
};
#endif
