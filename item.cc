#include "item.h"
#include "cell.h"
using namespace std;


Item::Item(char symbol,CellPtr cell): curCell {cell}, symbol{symbol} {}

char Item::getSymbol() {
	return symbol;
}

void Item::setCurCell(CellPtr cell) {
	curCell = cell;
}

Item::~Item() {}