#include "wattack.h"
#include "elf.h"
#include "player.h"
#include <iostream>
using namespace std;

const int attackAmt = 5;
const int zeroAttk = 0;

WAttack::WAttack(char symbol): Potion{symbol}{}

void WAttack::getPickedBy(PlayerPtr player){
	cout << "wattack getPickedBy" << endl;
	const int totalAttk = player->getAttk() + player->getTempAttk();
	if(totalAttk - attackAmt > 0)
		player->updateTempAttk(-1*attackAmt);
	
	else
		player->setTempAttk(-1 * player->getAttk()); // to nullify players actual attk. resultant gives 0 attk

	curCell->setItem(nullptr);
	cout << "cell set to nullptr" << endl;
	delete this;
}

void WAttack::getPickedBy(ElfPtr elf){
	cout << "wattack getPickedBy" << endl;
	elf->updateTempAttk(attackAmt);
	
	curCell->setItem(nullptr);
	cout << "cell set to nullptr" << endl;
	delete this;
}