#ifndef _PHOENIX_H_
#define _PHOENIX_H_

#include <memory>
#include "enemy.h"

class Phoenix:public Enemy{
public:
	Phoenix(int hp=50, int def=20, int attk=35, char symbol='X', int gold = 1);
};

#endif