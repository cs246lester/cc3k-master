#ifndef _POTIONFACT_H_
#define _POTIONFACT_H_
#include <string>
#include <memory>
class Potion;
typedef Potion* PotionPtr;

class PotionFactory{
public:
	PotionPtr generatePotion();
};

#endif