#include "bdefense.h"
#include "player.h"
#include "elf.h"
#include <iostream>
using namespace std;


const int defAmt = 5;

BDefense::BDefense(char symbol): Potion{symbol}{}

void BDefense::getPickedBy(PlayerPtr player){
	cout << "bdefense getPickedBy" << endl;

	player->updateTempDef(defAmt);

	curCell->setItem(nullptr);
	cout << "cell set to nullptr" << endl;
}

void BDefense::getPickedBy(ElfPtr elf){
	cout << "bdefense getPickedBy" << endl;

	elf->updateTempDef(defAmt);

	curCell->setItem(nullptr);
	cout << "cell set to nullptr" << endl;
}