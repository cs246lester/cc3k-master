#ifndef _DRAGONHORDE_H_
#define _DRAGONHORDE_H_

#include <memory>
#include "treasure.h"
class Player;
class Dwarf;
class Orc;
typedef Player* PlayerPtr;
typedef Orc* OrcPtr;
typedef Dwarf* DwarfPtr;

class Cell;
class Dragon;
typedef Cell* CellPtr;
typedef Dragon* DragonPtr;

class DragonHorde : public Treasure {
	bool canBePicked = false;
	DragonPtr dragon;
public:
	void attach(DragonPtr dragon);
	void notify();
	void notifyDragon();
	void detectIntrusion();
	void getPickedBy(PlayerPtr player) override;
	void getPickedBy(DwarfPtr) override;
	void getPickedBy(OrcPtr) override;
	DragonHorde(CellPtr cell, DragonPtr dragon);
};

#endif