#ifndef _NORMALHORDE_H_
#define _NORMALHORDE_H_
#include <string>
#include "treasure.h"
#include <memory>

class NormalHorde: public Treasure{
public:
	NormalHorde(int gold = 1);
};

#endif