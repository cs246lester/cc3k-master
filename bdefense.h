#ifndef _BDEF_H_
#define _BDEF_H_
#include <memory>
class Elf;
class Player;
typedef Player* PlayerPtr;
typedef Elf* ElfPtr;
#include "potion.h"

class BDefense:public Potion{
public:
	BDefense(char symbol='P');
	void getPickedBy(PlayerPtr player) override;
	void getPickedBy(ElfPtr elf) override;
};

#endif