#ifndef _RHEALTH_H
#define _RHEALTH_H

#include "potion.h"
#include <memory>
class Player;
class Elf;
typedef Player* PlayerPtr;
typedef Elf* ElfPtr;
class RHealth:public Potion{
public:
	RHealth(char symbol = 'P');
	void getPickedBy(PlayerPtr player) override;
	void getPickedBy(ElfPtr elf) override;
};

#endif