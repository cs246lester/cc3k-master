#ifndef _TEXTDISPLAY_H_
#define _TEXTDISPLAY_H_
#include <vector>
#include <memory>
class Cell;
class TextDisplay {
	const int rows = 25;
	const int cols = 79;

	std::vector< std::vector<char> > theDisplay;
public:
	TextDisplay();
	void notify(Cell& c);
	friend std::ostream& operator<<(std::ostream& out, const TextDisplay& td); 
};
#endif