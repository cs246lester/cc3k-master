#include "character.h"
#include "gamestate.h"
#include "cell.h"
#include <iostream>
using namespace std;

Character::Character(int hp, int def, int attk, char symbol, int gold): hp{hp}, def{def}, attk{attk}, symbol{symbol}, gold {gold} {
	
}

Character::~Character() {
	cout << "character dtor runs!" << endl;
}

int Character::getHp() const {
	return hp;
}

int Character::getDef() const {
	return def;
}

int Character::getAttk() const {
	return attk;
}

bool Character::isAlive() const {
	return alive;
}

void Character::setCurCell(CellPtr c){
	curCell = c;
}

char Character::getSymbol(){
	return symbol;
}

void Character::setHp(int health){
	if(health < 0){
		hp = 0;
		return;
	}
	
	hp = health;
}

void Character::setAttk(int attack){
	attk = attack;
}

void Character::addGold(int newGold) {
	gold += newGold;
}

void Character::setDef(int defense){
	def = defense;
}

void Character::setAlive(bool life){
	alive = life;
}


void Character::move(string dir){
	GameState state;
	CellPtr nextCell = curCell->getNeighbour(dir);
	CharacterPtr curChar(this);
	if (nextCell == nullptr) {
		cout<<"NULLPTR ENCOUNTERED IN MOVE FOR ENEMY!" << endl;
		return;
	}
	char cellType = nextCell->getSymbol();
	// TODO: Fix these cout statements (use ostringstream or something)
	if (cellType != '.') { 
		cout<<"NO EMPTY CELL ENCOUNTERED IN MOVE FOR ENEMY!" << endl;
		return;
	}
	nextCell->setCharacter(curChar);
	cout<<"removeChar beginning" <<endl;
	cout<< curCell->getCharacter()->getSymbol()<<endl;
	curCell->removeChar();
	cout<<"removeChar ending" <<endl;
	cout<<"curCell beginning"<< endl;
	curCell = nextCell;
	cout<<" curCell ending" <<endl;
	curChar->setCurCell(curCell);
	cout<<"Enemy has moved!!!"<<endl;
}

// void Character::attack(string dir){
// 	GameState state;
// 	CellPtr nextCell = curCell->getNeighbour(dir);
// 	if (nextCell == nullptr){
// 	 state.updateMessage(" ERROR! This tile doesnt exist!"); 
// 	 return;
// 	}

// 	CharacterPtr defender = nextCell->getCharacter();
// 	if (defender == nullptr) {
// 		state.updateMessage(" There is no enemy in that direction!");
// 		return;
// 	}

// 	defender->getAttackedBy(this);
// }