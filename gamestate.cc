#include "gamestate.h"
#include <string>
using namespace std;

GameState::GameState() {}

string GameState::message = "";
bool GameState::alive = true;
bool GameState::levelEnded = false;
bool GameState::gameEnded = false;

// Function to keep track of the action string
void GameState::updateMessage(string newMessage) {
	message += newMessage;
}

string GameState::getMessage() {
	return message;
}

void GameState::clearMessage() {
	message = "";
}

bool GameState::isAlive() {
	return alive;
}

void GameState::setAlive(bool life) {
	alive = life;
}

bool GameState::isLevelEnded() {
	return levelEnded;
}

void GameState::setLevelEnded(bool levelstat) {
	levelEnded = levelstat;
}

bool GameState::isGameEnded() {
	return gameEnded;
}

void GameState::setGameEnded(bool gameStatus) {
	gameEnded = gameStatus;
}