#include "cell.h"
#include "textdisplay.h"
#include "item.h"
#include "character.h"
#include <iostream>
using namespace std;
typedef Cell* CellPtr;
typedef Character* CharacterPtr;
// Cell ctor
Cell::Cell(int r, int c, char symbol): row {r}, col{c}, symbol {symbol}, curCharacter {nullptr}, curItem {nullptr}, td{nullptr} {}

void Cell::removeChar(){
	cout << "Accessing cur-character" << endl;
	if(curCharacter == nullptr) { cout << "LOL" << endl;}
	cout << curCharacter->getSymbol()<< endl;
	curCharacter = nullptr;
}

// getRow() accessor function
int Cell::getRow(){
	return row;
}

//getCol() accessor function
int Cell::getCol(){
	return col;
}

// function to set the co-ordinates
void Cell::setCoords(int r, int c){
	row = r;
	col = c;
}

void Cell::setSymbol(char sym) {
	symbol = sym;
}

void Cell::setItem(ItemPtr item) {
	cout << "setItem triggered" << endl;
	curItem = item;
}

char Cell::getSymbol(){
	if (curCharacter) {
		return curCharacter->getSymbol();
	} 
	if (curItem) 
		return curItem->getSymbol();

	return symbol;
}

void Cell::setCharacter(CharacterPtr c) {
	curCharacter = c;
	cout<< "SetCharacter done!" << endl;
}

CharacterPtr Cell::getCharacter(){
	return curCharacter;
}

ItemPtr Cell::getItem(){
	return curItem;
}

CellPtr Cell::getNeighbour(string dir) {
	if (neighbours.count(dir)) {
		return neighbours[dir];
	}
	else return nullptr;
}

void Cell::notifyDisplay() {
	td->notify(*this);
}

void Cell::attachNeighbour(CellPtr cell, string dir){
	neighbours[dir] = cell;
}

Cell::~Cell() {
	if (curCharacter) {
		delete curCharacter;
	} if (curItem) {
		delete curItem;
	}
}

void Cell::attachDisplay(TDPtr display){
	td = display;
}