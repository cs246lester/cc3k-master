#ifndef _GOBLIN_H_
#define _GOBLIN_H_
#include "enemy.h"
#include <memory>
class Goblin : public Enemy {
public:
	Goblin(int hp=70, int attk=5, int def=10, char symbol='N', int gold=1);
};
#endif
