#include "cellfactory.h"
#include "cell.h"
using namespace std;

CellPtr CellFactory::generateCell(int row, int col, char symbol) {
	CellPtr cell = new Cell (row, col, symbol);
	return cell;
}