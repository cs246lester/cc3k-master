#ifndef _PLAYERFACTORY_H_
#define _PLAYERFACTORY_H_
#include <string>
#include <memory>
class Player;
typedef Player* PlayerPtr;

class PlayerFactory{

public:
	PlayerPtr generatePlayer(char race);

};

#endif