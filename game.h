#ifndef _GAME_H_
#define _GAME_H_
#include <string>
#include <vector>
#include <memory>
class Floor;
class Player;
class TextDisplay;
class Cell;
class Enemy;
typedef Cell* CellPtr;
typedef Floor* FloorPtr;
typedef Player* PlayerPtr;
typedef Enemy* EnemyPtr;
typedef TextDisplay* TDPtr;

class Game {
	std::string filename;
	std::vector< std::vector<CellPtr> > theGrid;
	TDPtr td;
	PlayerPtr pc;
	const int rows = 25;
	const int cols = 79;
	FloorPtr floor;
public:
	Game(PlayerPtr pc, std::string filename="initfloor.txt");
	void respawn();
	void notifyDisplay();
	void printGrid();
	void updateState();
	friend std::ostream& operator << (std::ostream& out, Game& game);
	~Game();
};
#endif