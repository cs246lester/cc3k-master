#include <iostream>
#include "game.h"
#include "player.h"
#include "playerfactory.h"
#include "gamestate.h"
#include <string>
#include <vector>
#include <sstream>
#include <random>

using namespace std;

typedef Player* PlayerPtr;

int main(int argc, char* argv[]) {
	srand(time(0));
	PlayerFactory playerFactory;
	while (true) {
		char race;
		cout << "Welcome to Chamber Crawler 3000!" << endl;
		cout << "Please enter the race you want to start with:" << endl;
		cout << "1. (h)uman" << endl;
		cout << "2. (e)lf" << endl;
		cout << "3. (d)warf" << endl;
		cout << "4. (o)rc" << endl;

		cin >> race; 
		while (race != 'h' and race != 'o' and race != 'd' and race != 'e') {
			cout << "Please enter one of the above specified races!" << endl;
			cin >> race;
		}
		PlayerPtr pc = playerFactory.generatePlayer(race);
		// Instantiating the Game class
		Game game{pc};
		game.notifyDisplay();
		cout << game;

		string command;
		GameState state;
		while (true) {
			cin >> command;
			if (command == "no" || command == "so" || command == "ea" || command == "we" 
				|| command == "ne" || command == "nw" || command == "se" || command == "sw") {			
				pc->move(command);
				game.updateState();
				game.notifyDisplay();
				cout << game;
				cout << state.getMessage() << endl;
				state.clearMessage();
			}
			else if (command == "u") {
				string dir;
				cin >> dir;
				pc->pick(dir);
				game.updateState();
				game.notifyDisplay();
				cout << game;
				cout << state.getMessage() << endl;
				state.clearMessage();
			}
			else if (command == "a") {
				string dir;
				cin >> dir;
				pc->attack(dir);
				game.updateState();
				game.notifyDisplay();
				cout << game;
				cout << state.getMessage() << endl;
				state.clearMessage();
			}
			else if (command == "r") {
				break;
			}
			else if (command == "q") {
				return 0;
			}
		}
		delete pc;
	}
}