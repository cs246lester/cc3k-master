#include "battack.h"
#include "elf.h"
#include "player.h"
#include <iostream>
using namespace std;

typedef Player* PlayerPtr;
typedef Elf* ElfPtr;
const int attackAmt = 5;

BAttack::BAttack(char symbol): Potion{symbol}{}

void BAttack::getPickedBy(PlayerPtr player){
	cout << "battack getPickedBy" << endl;
	player->updateTempAttk(attackAmt);

	curCell->setItem(nullptr);
	cout << "cell set to nullptr" << endl;
}

void BAttack::getPickedBy(ElfPtr elf){
	cout << "battack getPickedBy" << endl;
	elf->updateTempAttk(attackAmt);

	curCell->setItem(nullptr);
	cout << "cell set to nullptr" << endl;
}