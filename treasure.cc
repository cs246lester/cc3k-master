#include "treasure.h"
#include "orc.h"
#include "dwarf.h"
#include "player.h"

#include <iostream>
using namespace std;
typedef Character* CharacterPtr;

int Treasure::getHordeVal(){
	return hordeVal;
}


Treasure::Treasure(int gold,CellPtr cell, char symbol):hordeVal{gold}, Item{symbol,cell} {

}

void Treasure::getPickedBy(PlayerPtr player){
	player->addGold(hordeVal);
	setCurCell(nullptr);
	delete this;
}

void Treasure::getPickedBy(DwarfPtr dwarf){
	dwarf->addGold(2*hordeVal);
	setCurCell(nullptr);
	delete this;
}

void Treasure::getPickedBy(OrcPtr orc){
	orc->addGold(0.5*hordeVal);
	setCurCell(nullptr);
	delete this;
}

Treasure::~Treasure() {}