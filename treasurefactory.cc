#include "treasurefactory.h"
#include "treasure.h"
#include "nhorde.h"
#include "mhorde.h"
#include "dhorde.h"
#include "shorde.h"
#include <ctime>
#include <cstdlib>
using namespace std;

TreasurePtr TreasureFactory::generateTreasure(){
	int TreasureNum = rand() % 7;
	if(TreasureNum == 0 || TreasureNum == 1 || TreasureNum == 2 || TreasureNum == 3 || TreasureNum == 4){
		TreasurePtr t = new NormalHorde ();
		return t;
	}
	else {
		TreasurePtr t = new SmallHorde ();
		return t;
	}
}